package com.adacms.test;

import com.openyelp.data.entity.Shop;
import com.openyelp.data.entity.ShopReview;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.data.service.ShopReviewService;
import com.openyelp.data.service.ShopService;

public class ShopReviewTest extends BaseTest {

	/**
	 * Rigourous Test :-)
	 */
	public void testAdd() {
		ShopReview shopreview = new ShopReview();
		ShopReviewService service = applicationContext.getBean(ShopReviewService.class);
		service.save(shopreview);
		assertNull(shopreview.getId());
		
		
		
		Shop shop1=new Shop();
		shop1.setId(3);
		shopreview.setShop(shop1);
		UserInfo user=new UserInfo();
		user.setId(1l);
		shopreview.setUser(user);
		service.save(shopreview);
		assertNotNull(shopreview);
		service.deleteById(shopreview.getId());
		shopreview = service.findById(shopreview.getId());
		assertNull(shopreview);


	}

	/**
	 * Rigourous Test :-)
	 */
	public void testTag() {
		assertTrue(true);

		Shop shop = new Shop();
		ShopService service = applicationContext.getBean(ShopService.class);
		shop=service.findById(3);
		shop = service.addTag(shop, "酒店3");
		assertNotNull(shop.getId());
        assertEquals(shop.getTags().size(), 3);
		shop = service.deleteTag(shop, "酒店3");
        assertEquals(shop.getTags().size(), 2);


	}
}
