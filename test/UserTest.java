package com.adacms.test;

import com.openyelp.data.entity.ShopReview;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.data.service.ShopReviewService;
import com.openyelp.data.service.UserService;

import junit.framework.TestCase;

public class UserTest extends BaseTest {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testApp() {
		assertTrue(true);

		UserInfo shopreview = new UserInfo();
		shopreview.setUsername("ada1");
		shopreview.setPlainPassword("123456");
		UserService service = applicationContext.getBean(UserService.class);
		service.save(shopreview);
		assertNotNull(shopreview.getId());
		service.deleteById(shopreview.getId());

	}
}
