package com.adacms.test;

import com.openyelp.data.entity.EventInfoCategory;
import com.openyelp.data.service.EventInfoCategoryService;

public class TestInfoCategoryService extends BaseTest {

	public void testApp() {
		assertTrue(true);

		EventInfoCategory shopreview = new EventInfoCategory();
		shopreview.setName("音乐");
		EventInfoCategory parent=new EventInfoCategory();;
		parent.setId(1);
		shopreview.setParent(parent);
		EventInfoCategoryService service = applicationContext
				.getBean(EventInfoCategoryService.class);
		service.save(shopreview);
		assertNotNull(shopreview.getId());

	}
}
