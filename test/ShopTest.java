package com.adacms.test;

import com.openyelp.data.entity.Shop;
import com.openyelp.data.entity.ShopReview;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.data.service.ShopReviewService;
import com.openyelp.data.service.ShopService;

public class ShopTest extends BaseTest {

	/**
	 * Rigourous Test :-)
	 */
	public void testAdd() {
		assertTrue(true);


	}

	/**
	 * Rigourous Test :-)
	 */
	public void testTag() {
		assertTrue(true);

		Shop shop = new Shop();
		ShopService service = applicationContext.getBean(ShopService.class);
		shop=service.findById(3);
		shop = service.addTag(shop, "酒店3");
		assertNotNull(shop.getId());
        assertEquals(shop.getTags().size(), 3);
		shop = service.deleteTag(shop, "酒店3");
        assertEquals(shop.getTags().size(), 2);


	}
}
