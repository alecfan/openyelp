package com.adacms.test;

import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import com.openyelp.data.entity.Area;
import com.openyelp.data.entity.Shop;
import com.openyelp.data.service.AreaService;
import com.openyelp.data.service.ShopService;
import com.openyelp.lbs.webservice.domain.BackPoi;
import com.openyelp.lbs.webservice.domain.Backs;
import com.openyelp.lbs.webservice.v2.PlaceService;

public class BaiduLbsShopTest extends BaseTest {
	
	
	
	public void pp() {
		ShopService service = applicationContext.getBean(ShopService.class);
		AreaService areaservice = applicationContext.getBean(AreaService.class);
		List<Area> as = areaservice.findByLevel(3);
		for (Area area2 : as) {
			try {
				PlaceService s = new PlaceService();
				s.setRegion(area2.getName());
				for (int i = 1; i < 2000; i++) {
					Backs b = s.searchByKey("美食", 10, i);

					add(service, areaservice, b, area2);

					if (b == null || b.getResults() == null
							|| b.getResults().size() < 8) {
						break;
					}
					System.out.println(b);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	private void add(ShopService service, AreaService areaservice, Backs b,
			Area area) {
		if (b != null) {
			List<BackPoi> pois = b.getResults();
			for (BackPoi backPoi : pois) {
				System.out.println(backPoi);
				Shop shop = new Shop();
				shop.setAddDate(new Date());
				shop.setAddress(backPoi.getAddress());
				if (backPoi.getDetail_info() != null) {
					shop.setWebsite(backPoi.getDetail_info().getDetail_url());
				}
				if (backPoi.getLocation() != null) {
					shop.setGps(backPoi.getLocation().getgps());
					shop.setLatitude((double) backPoi.getLocation().getLat());
					shop.setLongitude((double) backPoi.getLocation().getLng());

					shop.setArea(area);
					shop.setPhone(backPoi.getTelephone());
					shop.setName(backPoi.getName());
					service.save(shop);
					if (backPoi.getDetail_info() != null) {
						shop.setWebsite(backPoi.getDetail_info()
								.getDetail_url());
						String tags = backPoi.getDetail_info().getTag();
						if (tags != null) {
							String[] ts = tags.split(";");
							for (String string : ts) {
								service.addTag(shop, string);
							}
						}
					}
				}

			}
		}
	}

}
