openyelp
======
##一个简单的开源评论系统##
前期主要完成和美国yelp一样的功能，后期会增加一些特色功能。
###演示地址:[http://openyelp.aliapp.com/](http://openyelp.aliapp.com/talk.htm)
账号 ada  密码123456
###spring+springmvc+hibernate+shiro+maven+bootstrap
#####联系方式：2601035599@qq.com
## qq交流群：399591308 ##
[![openyelp交流](http://pub.idqqimg.com/wpa/images/group.png)](http://shang.qq.com/wpa/qunwpa?idkey=f6a41245aa9e46be1018f5b9359f6c6a2d28d86497d5e4a6d5bb800c4a0346fd "qq 群")
#ps：欢迎大家访问我的淘宝话费店 [http://1688hf.taobao.com/](http://1688hf.taobao.com/ "http://1688hf.taobao.com/")#

#网站首页#
![](http://openyelp.aliapp.com/assets/images/site.png)
# 主要功能 #
- 评论系统
- 聊天系统
- 活动系统
- 推荐系统

# 项目部署 #
1. 修改src/main/resources/jdbc.properties
2. 通过maven运行mvn tomcat7:run
3. 通过双击start.bat  需要修改里面的工程位置 cd  /d  F:\webworkspaces\openyelp 改为你自己的工程位置

