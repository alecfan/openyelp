package com.openyelp.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ada.common.action.BaseAction;
import com.openyelp.web.utils.FrontUtils;


@Controller
@RequestMapping(value = "user")
public class UserAction extends BaseAction {
	
	


	
	@RequestMapping(value = "find_friends", method = RequestMethod.GET)
	public String find_friends(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("user/find_friends");
	}
	@RequestMapping(value = "messages", method = RequestMethod.GET)
	public String messages(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("user/messages");
	}
}
