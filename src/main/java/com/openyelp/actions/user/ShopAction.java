package com.openyelp.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.openyelp.data.entity.Shop;
import com.openyelp.data.service.ShopService;
import com.openyelp.web.utils.FrontUtils;


@Controller
@RequestMapping(value = "user")
public class ShopAction {

	@Autowired
	private ShopService shopService;
	@RequestMapping(value = "writeareview", method = RequestMethod.GET)
	public String writeareview(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("user/writeareview");
	}
	@RequestMapping(value = "search", method = RequestMethod.GET)
	public String searchareview(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("user/search");
	}
	@RequestMapping(value = "newbiz", method = RequestMethod.GET)
	public String newbiz(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("user/newbiz");
	}
	
	@RequestMapping(value = "newbiz", method = RequestMethod.POST)
	public String newbizwork(Shop shop,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {
		
		shopService.save(shop);
		return FrontUtils.getPath("user/newbiz");
	}
}
