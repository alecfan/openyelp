package com.openyelp.actions.front;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.openyelp.data.apps.ObjectFactory;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.Area;
import com.openyelp.data.entity.EntityContent;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.data.service.AreaService;
import com.openyelp.data.service.CommentService;
import com.openyelp.data.service.EntityContentService;
import com.openyelp.data.service.ShopService;
import com.openyelp.data.service.UserService;
import com.openyelp.services.haoservice.lifeservice.joke.ContentList;
import com.openyelp.services.haoservice.lifeservice.joke.JokeApi;
import com.openyelp.services.haoservice.lifeservice.joke.JokeContent;
import com.openyelp.shiro.utils.UserUtil;
import com.openyelp.web.utils.FrontUtils;

@Controller
public class SiteAction {

	@Autowired
	CommentService commentService;

	@Autowired
	AreaService areaService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {
		List<Area> as = areaService.findByParent(UserUtil.getCurrentCity()
				.getId());
		Pagination page = commentService.getPage(curpage, pagesize);
		model.addAttribute("page", page);
		model.addAttribute("list", page.getList());
		model.addAttribute("curpage", curpage);
		model.addAttribute("pagesize", pagesize);
		model.addAttribute("towns", as);
		List<Area> a1 = new ArrayList<Area>();
		List<Area> a2 = new ArrayList<Area>();
		List<Area> a3 = new ArrayList<Area>();
		List<Area> a4 = new ArrayList<Area>();

		if (as != null && as.size() > 0) {
			for (int i = 0; i < as.size(); i++) {
				if (i > 0) {
					
					if(i%4==0){
						a1.add(as.get(i));
					}
					if(i%4==1){
						a2.add(as.get(i));
					}
					if(i%4==2){
						a3.add(as.get(i));
					}
					if(i%4==3){
						a4.add(as.get(i));
					}

				} else if (i == 0) {
					a1.add(as.get(i));
				}
			}

		}
		model.addAttribute("towns1", a1);
		model.addAttribute("towns2", a2);
		model.addAttribute("towns3", a3);
		model.addAttribute("towns4", a4);

		return FrontUtils.getPath("index");
	}

	@RequestMapping(value = "user_details", method = RequestMethod.GET)
	public String user_details(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("user/user_details");
	}

	@RequestMapping(value = "events", method = RequestMethod.GET)
	public String events(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("events");
	}

	@RequestMapping(value = "locations", method = RequestMethod.GET)
	public String locations(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("locations");
	}

	@Autowired
	ShopService shopService;
	@RequestMapping(value = "search", method = RequestMethod.GET)
	public String search(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {
		Pagination rs = shopService.getPage(curpage, pagesize);
		if (rs != null) {
			model.addAttribute("list", rs.getList());
			model.addAttribute("page", rs);
		}
		model.addAttribute("curpage", curpage);

		model.addAttribute("pagesize", pagesize);
		return FrontUtils.getPath("search");
	}

	@RequestMapping(value = "guidelines", method = RequestMethod.GET)
	public String guidelines(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("guidelines");
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("login");
	}

	@RequestMapping(value = "signup", method = RequestMethod.GET)
	public String signup(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		return FrontUtils.getPath("signup");
	}

	@RequestMapping(value = "joke", method = RequestMethod.GET)
	public String joke(
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		EntityContentService s = ObjectFactory.get().getBean(
				EntityContentService.class);
		ContentList list = JokeApi.ContentList(curpage, pagesize);
		List<JokeContent> imgs = list.getResult();
		for (JokeContent jokeContent : imgs) {
			EntityContent e = new EntityContent();
			e.setContent(jokeContent.getContent());
			e.setType(jokeContent.getType());
			e.setTitle(jokeContent.getTitle());
			e.setUpdatetime(jokeContent.getUpdatetime());
			s.save(e);
		}
		return "index";
	}

	@RequestMapping(value = "login1", method = RequestMethod.GET)
	public String login1(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return "login";
	}

	@RequestMapping(value = "demo", method = RequestMethod.GET)
	public @ResponseBody String demo(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String result = "demo";
		return result;
	}

	@RequestMapping(value = "downring", method = RequestMethod.GET)
	public String downring(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		try {
			String html = Jsoup
					.connect(
							"http://91mydoor.com/downring/DownRing?type=2&star=0&end=10")
					.execute().body();
			model.addAttribute("msg", html);
		} catch (IOException e) {
			e.printStackTrace();
			model.addAttribute("msg", "erro");
		}

		return "msg";
	}

	@Autowired
	UserService userService;

	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public String signup(HttpServletRequest request, UserInfo user,
			HttpServletResponse response, Model model) {
		user.setEmail("");
		user.setAddDate(new Date());
		user.setLastDate(new Date());
		UserInfo u = userService.save(user);
		if (u.getId() < 0) {
			model.addAttribute("msg", "该用户已经注册过了");
			return FrontUtils.getPath("signup");
		} else {
			return FrontUtils.getPath("login");

		}
	}
}
