package com.openyelp.actions.front;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ada.common.action.BaseAction;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.Talk;
import com.openyelp.data.entity.TalkReply;
import com.openyelp.data.service.TalkReplyService;
import com.openyelp.data.service.TalkService;
import com.openyelp.shiro.utils.UserUtil;
import com.openyelp.web.utils.FrontUtils;

@Controller
public class TalkAction extends BaseAction {

	@Autowired
	TalkService talkService;

	@RequiresUser
	@RequestMapping(value = "talk/new_topic", method = RequestMethod.GET)
	public String events(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("talk/new_topic");
	}

	@RequestMapping(value = "talk", method = RequestMethod.GET)
	public String talk(
			@RequestParam(value = "id", required = true, defaultValue = "100") int id,
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "category", required = true, defaultValue = "-1") int category,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		Pagination rs = talkService.findByCity(UserUtil.getCurrentCity()
				.getId(), category, curpage, pagesize);
		if (rs != null) {
			model.addAttribute("list", rs.getList());
			model.addAttribute("page", rs);
		}
		model.addAttribute("curpage", curpage);
		model.addAttribute("category", category);

		model.addAttribute("id", id);
		model.addAttribute("pagesize", pagesize);
		return FrontUtils.getPath("talk");
	}
	
	
	@RequiresUser
	@RequestMapping(value = "mytalk", method = RequestMethod.GET)
	public String mytalk(
			@RequestParam(value = "id", required = true, defaultValue = "100") int id,
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "category", required = true, defaultValue = "-2") int category,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		Pagination rs = talkService.findByCity(UserUtil.getCurrentCity()
				.getId(), category,UserUtil.getCurrentUser().getId(), curpage, pagesize);
		if (rs != null) {
			model.addAttribute("list", rs.getList());
			model.addAttribute("page", rs);
		}
		model.addAttribute("curpage", curpage);
		model.addAttribute("category", category);

		model.addAttribute("id", id);
		model.addAttribute("pagesize", pagesize);
		return FrontUtils.getPath("mytalk");
	}
	
	@RequiresUser
	@RequestMapping(value = "talk/talk", method = RequestMethod.POST)
	public String talk(HttpServletRequest request, Talk talk, int category,
			String location, HttpServletResponse response, Model model) {
		// UserUtil
		talk.setAdddate(new Date());
		talk.setUser(UserUtil.getCurrentUser());
		talk = talkService.add(talk, category, location);
		if (talk.getId() > 0) {
			return FrontUtils.redirect("topic.htm?id=" + talk.getId());

		} else {
			model.addAttribute("msg", "请选择城市");
			return FrontUtils.getPath("talk/new_topic");

		}
	}

	@Autowired
	TalkReplyService talkReplyService;

	@RequiresUser
	@RequestMapping(value = "talk/create_reply", method = RequestMethod.POST)
	public String create_reply(HttpServletRequest request, TalkReply talkReply,
			int category, String location, HttpServletResponse response,
			Model model) {
		// UserUtil
		talkReply.setAdddate(new Date());
		talkReply.setUser(UserUtil.getCurrentUser());
		talkReply = talkReplyService.save(talkReply);
		if (talkReply.getId() > 0) {
			return FrontUtils.redirect("topic.htm?id="
					+ talkReply.getTalk().getId());
		} else {
			model.addAttribute("msg", "请选择城市");
			return FrontUtils.getPath("talk/new_topic");

		}
	}
	
	@RequestMapping(value = "talk/topic", method = RequestMethod.GET)
	public String topic(
			HttpServletRequest request,
			Integer id,
			@RequestParam(value = "curpage", required = true, defaultValue = "1") int curpage,
			@RequestParam(value = "pagesize", required = true, defaultValue = "10") int pagesize,
			@RequestParam(value = "category", required = true, defaultValue = "-1") int category,
			HttpServletResponse response, Model model) {
		Talk talk = talkService.findById(id);
		model.addAttribute("item", talk);
		model.addAttribute("category", category);
		Pagination rs = talkReplyService.pageByTalk(id, curpage, pagesize);

		model.addAttribute("list", rs.getList());
		model.addAttribute("page", rs);
		model.addAttribute("curpage", curpage);
		model.addAttribute("category", category);

		model.addAttribute("id", id);
		model.addAttribute("pagesize", pagesize);
		return FrontUtils.getPath("talk/topic");
	}

}
