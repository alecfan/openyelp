package com.openyelp.actions.front;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.openyelp.data.entity.EventInfo;
import com.openyelp.data.service.EventInfoCategoryService;
import com.openyelp.data.service.EventInfoService;
import com.openyelp.web.utils.FrontUtils;
import com.openyelp.web.webbinding.CustomTimestampEditor;

@Controller
@RequestMapping(value = "events")
public class EventsAction {

	
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);

		SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd");
		datetimeFormat.setLenient(false);

		binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(
				dateFormat, true));
		binder.registerCustomEditor(java.sql.Timestamp.class,
				new CustomTimestampEditor(datetimeFormat, true));
	}
	
	@Autowired
	EventInfoService eventInfoService;
	@Autowired
	EventInfoCategoryService eventInfoCategoryService;

	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String create(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		model.addAttribute("list", eventInfoCategoryService.findByPid(1));
		return FrontUtils.getPath("events/create");
	}

	@RequestMapping(value = "create", method = RequestMethod.POST)
	public String creatework(HttpServletRequest request,
			HttpServletResponse response, EventInfo info, Model model) {

		eventInfoService.save(info);

		return FrontUtils.getPath("events/addphoto");
	}

	@RequestMapping(value = "view", method = RequestMethod.GET)
	public String view(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("events/view");
	}
}
