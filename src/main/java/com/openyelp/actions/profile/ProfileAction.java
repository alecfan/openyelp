package com.openyelp.actions.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.openyelp.core.web.WebErrors;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.data.entity.UserProfile;
import com.openyelp.data.service.UserProfileService;
import com.openyelp.data.service.UserService;
import com.openyelp.shiro.utils.UserUtil;
import com.openyelp.web.utils.FrontUtils;

@Controller
@RequestMapping(value = "profile")
public class ProfileAction {

	@Autowired
	UserProfileService userProfileService;
	
	
	
	@RequestMapping(value = "profile_bio", method = RequestMethod.GET)
	public String profile_bio(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		UserProfile profile=UserUtil.getCurrentUser().getProfile();
		if(profile==null){
			profile=new UserProfile();
		}else{
			profile=userProfileService.findById(profile.getId());
		}
		model.addAttribute("profile", profile);
		return FrontUtils.getPath("profile/profile_bio");
	}
	@RequestMapping(value = "profile_bio", method = RequestMethod.POST)
	public String profile_bioupdate(HttpServletRequest request,UserProfile profile,
			HttpServletResponse response, Model model) {

		profile.setUser(UserUtil.getCurrentUser());
		userProfileService.update(profile);
		UserUtil.getCurrentUser().setProfile(profile);
		model.addAttribute("message", "你的个人资料已被更新！");
		return FrontUtils.getPath("profile/profile");	}
	@RequestMapping(value = "user_photos", method = RequestMethod.GET)
	public String user_photos(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/user_photos");
	}

	@RequestMapping(value = "profile_friends_invited", method = RequestMethod.GET)
	public String profile_friends_invited(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/profile_friends_invited");
	}

	@RequestMapping(value = "profile", method = RequestMethod.GET)
	public String profile(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/profile");
	}

	@RequestMapping(value = "profile_password", method = RequestMethod.GET)
	public String profile_password(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/profile_password");
	}

	@RequestMapping(value = "profile_password", method = RequestMethod.POST)
	public String profile_passwordwork(HttpServletRequest request,
			String confirm_password, String password, String oldpassword,
			HttpServletResponse response, Model model) {

		WebErrors errors = WebErrors.create(request);
		if (password == null) {
			errors.addErrorString("新密码不能为空");
		}
		if (oldpassword == null) {
			errors.addErrorString("老密码不能为空");
		}
		if (confirm_password == null) {
			errors.addErrorString("核实密码不能为空");
		}
		if (password != null) {
			if (!password.equals(confirm_password)) {
				errors.addErrorString("两次密码不一致");
			}
		}
		if(errors.getCount()==0){
			UserInfo u = userService.updatePassword(UserUtil.getCurrentShiroUser()
					.getId(), oldpassword, password);
			if (u.getId() == -1) {
				errors.addErrorString("老密码不正确");
			}
			if (u.getId() == -2) {
				errors.addErrorString("两次密码不一致");

			}
			if (u.getId() > 0) {
				model.addAttribute("message", "修改密码成功");
				return FrontUtils.getPath("profile/profile");

			}
		}else{
			
		}

		errors.toModel(model);
		return FrontUtils.getPath("profile/profile_password");
	}

	@Autowired
	UserService userService;

	@RequestMapping(value = "profile_email_notifications", method = RequestMethod.GET)
	public String profile_email_notifications(String old_password,
			String password, String confirm_password,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {
		userService.updatePassword(UserUtil.getCurrentShiroUser().getId(),
				old_password, password);
		return FrontUtils.getPath("profile/profile_email_notifications");
	}



	@RequestMapping(value = "profile_friends", method = RequestMethod.GET)
	public String profile_friends(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/profile_friends");
	}

	@RequestMapping(value = "profile_sharing", method = RequestMethod.GET)
	public String profile_sharing(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/profile_sharing");
	}

	@RequestMapping(value = "profile_privacy", method = RequestMethod.GET)
	public String profile_privacy(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return FrontUtils.getPath("profile/profile_privacy");
	}
}
