package #{dao_p};


import  com.openyelp.data.core.BaseDao;
import  com.openyelp.data.core.Updater;
import com.openyelp.data.core.Pagination;
import  #{entity_p}.#{Entity};

public interface #{Entity}Dao extends BaseDao<#{Entity}, #{idtype}>{
	public Pagination getPage(int pageNo, int pageSize);

	public #{Entity} findById(#{idtype} id);

	public #{Entity} save(#{Entity} bean);

	public #{Entity} updateByUpdater(Updater<#{Entity}> updater);

	public #{Entity} deleteById(#{idtype} id);
}