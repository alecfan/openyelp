package #{manager_impl_p};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import #{dao_p}.#{Entity}Dao;
import #{entity_p}.#{Entity};
import #{manager_p}.#{Entity}Service;

@Service
@Transactional
public class #{Entity}ServiceImpl implements #{Entity}Service {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public #{Entity} findById(#{idtype} id) {
		#{Entity} entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public #{Entity} save(#{Entity} bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public #{Entity} update(#{Entity} bean) {
		Updater<#{Entity}> updater = new Updater<#{Entity}>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public #{Entity} deleteById(#{idtype} id) {
		#{Entity} bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public #{Entity}[] deleteByIds(#{idtype}[] ids) {
		#{Entity}[] beans = new #{Entity}[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private #{Entity}Dao dao;

	@Autowired
	public void setDao(#{Entity}Dao dao) {
		this.dao = dao;
	}
}