package com.openyelp.shiro.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.openyelp.data.entity.Area;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.shiro.realm.UserRealm.ShiroUser;

public class UserUtil {
	/**
	 * 获取当前用户对象shiro
	 * 
	 * @return shirouser
	 */
	public static ShiroUser getCurrentShiroUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}

	/**
	 * 获取当前用户session
	 * 
	 * @return session
	 */
	public static Session getSession() {
		Session session = SecurityUtils.getSubject().getSession();
		return session;
	}

	/**
	 * 获取当前用户httpsession
	 * 
	 * @return httpsession
	 */
	public static Session getHttpSession() {
		Session session = SecurityUtils.getSubject().getSession();
		return session;
	}

	/**
	 * 获取当前用户对象
	 * 
	 * @return user
	 */
	public static UserInfo getCurrentUser() {
		Session session = SecurityUtils.getSubject().getSession();
		if (null != session) {
			return (UserInfo) session.getAttribute("user");
		} else {
			return null;
		}

	}

	public static UserInfo setCurrentUser(UserInfo user) {
		Session session = SecurityUtils.getSubject().getSession();
		if (null != session) {
			session.setAttribute("user", user);
			return (UserInfo) session.getAttribute("user");
		} else {
			return null;
		}

	}

	public static Area setCurrentCity(Area area) {
		Session session = SecurityUtils.getSubject().getSession();
		if (null != session) {
			session.setAttribute("area", area);
			return (Area) session.getAttribute("area");
		} else {
			return area;
		}

	}

	public static Area getCurrentCity() {
		Session session = SecurityUtils.getSubject().getSession();
		if (null != session) {
			Area e = (Area) session.getAttribute("area");
			if (e == null) {
				e = new Area();
				e.setId(100);
				e.setName("西安");
			}
			return e;
		} else {
			Area e = new Area();
			e.setId(100);
			e.setName("西安");
			return e;
		}

	}
}
