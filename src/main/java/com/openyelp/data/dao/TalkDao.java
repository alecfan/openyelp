package com.openyelp.data.dao;


import  com.openyelp.data.core.BaseDao;
import  com.openyelp.data.core.Updater;
import com.openyelp.data.core.Pagination;
import  com.openyelp.data.entity.Talk;

public interface TalkDao extends BaseDao<Talk, Integer>{
	public Pagination getPage(int pageNo, int pageSize);

	public Talk findById(Integer id);

	public Talk save(Talk bean);

	public Talk updateByUpdater(Updater<Talk> updater);

	public Talk deleteById(Integer id);
}