package com.openyelp.data.dao;


import  com.openyelp.data.core.BaseDao;
import  com.openyelp.data.core.Updater;
import com.openyelp.data.core.Pagination;
import  com.openyelp.data.entity.ShopCategory;

public interface ShopCategoryDao extends BaseDao<ShopCategory, Integer>{
	public Pagination getPage(int pageNo, int pageSize);

	public ShopCategory findById(Integer id);

	public ShopCategory save(ShopCategory bean);

	public ShopCategory updateByUpdater(Updater<ShopCategory> updater);

	public ShopCategory deleteById(Integer id);
}