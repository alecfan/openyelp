package com.openyelp.data.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.openyelp.data.core.BaseDaoImpl;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.dao.ShopCategoryDao;
import com.openyelp.data.entity.ShopCategory;

@Repository
public class ShopCategoryDaoImpl extends BaseDaoImpl<ShopCategory, Integer> implements ShopCategoryDao {
	public Pagination getPage(int pageNo, int pageSize) {
		Criteria crit = createCriteria();
		Pagination page = findByCriteria(crit, pageNo, pageSize);
		return page;
	}

	public ShopCategory findById(Integer id) {
		ShopCategory entity = get(id);
		return entity;
	}

	public ShopCategory save(ShopCategory bean) {
		getSession().save(bean);
		return bean;
	}

	public ShopCategory deleteById(Integer id) {
		ShopCategory entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopCategory> getEntityClass() {
		return ShopCategory.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}