package com.openyelp.data.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.openyelp.data.core.BaseDaoImpl;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.dao.AreaDao;
import com.openyelp.data.entity.Area;

@Repository
public class AreaDaoImpl extends BaseDaoImpl<Area, Integer> implements AreaDao {
	public Pagination getPage(int pageNo, int pageSize) {
		Criteria crit = createCriteria();
		Pagination page = findByCriteria(crit, pageNo, pageSize);
		return page;
	}

	public Area findById(Integer id) {
		Area entity = get(id);
		return entity;
	}

	public Area save(Area bean) {
		getSession().save(bean);
		return bean;
	}

	public Area deleteById(Integer id) {
		Area entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Area> getEntityClass() {
		return Area.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}