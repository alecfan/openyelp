package com.openyelp.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "eventinfo_comment")
public class EventInfoComment {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	@ManyToOne
	@JoinColumn(name="userid",unique = true)
	private UserInfo user;
	
	
	@ManyToOne
	@JoinColumn(name="eventInfoid",unique = true)
	private EventInfo eventInfo;
}
