package com.openyelp.data.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "shop")
public class Shop {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;
	
	@Column(length=100)
	private String address;
	
	
	
	@Column(length=100)
	private String gps;
	@Column(length=100)
	private String name;
	@Column(length=100)
	private String phone;
	
	@Column(length=100)
	private String website;
	/**
	 * 经度
	 */
	private Double longitude; 
	
	/**
	 * 纬度
	 */
	private Double latitude; 
	
	private Date addDate;
	
	private Date lastDate;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "shop_tag_links")
	private Set<ShopTag> tags = new HashSet<ShopTag>();
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="userid")
	private UserInfo user;
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="areaid")
	private Area area;

	public Date getAddDate() {
		return addDate;
	}

	public String getAddress() {
		return address;
	}

	public Area getArea() {
		return area;
	}

	public String getGps() {
		return gps;
	}

	public Integer getId() {
		return id;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public Set<ShopTag> getTags() {
		return tags;
	}

	public UserInfo getUser() {
		return user;
	}

	public String getWebsite() {
		return website;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public void setGps(String gps) {
		this.gps = gps;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setTags(Set<ShopTag> tags) {
		this.tags = tags;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

}
