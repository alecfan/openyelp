package com.openyelp.data.service;

import java.util.List;

import com.openyelp.annotation.RestFul;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.Area;
@RestFul(value="AreaService",api=AreaService.class)
public interface AreaService {
	public Pagination getPage(int pageNo, int pageSize);

	public Area findById(Integer id);

	public Area findByName(String name);

	public List<Area> findByLevel(Integer id);

	public List<Area> findByParent(Integer id);

	public List<Area> findByHot(Integer id);

	public Area save(Area bean);

	public Area update(Area bean);

	public Area deleteById(Integer id);

	public Area[] deleteByIds(Integer[] ids);
}