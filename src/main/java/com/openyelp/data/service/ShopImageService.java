package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.ShopImage;

public interface ShopImageService {
	public Pagination getPage(int pageNo, int pageSize);

	public ShopImage findById(Long id);

	public ShopImage save(ShopImage bean);

	public ShopImage update(ShopImage bean);

	public ShopImage deleteById(Long id);
	
	public ShopImage[] deleteByIds(Long[] ids);
}