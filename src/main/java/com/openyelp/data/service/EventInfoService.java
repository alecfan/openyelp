package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.EventInfo;

public interface EventInfoService {
	public Pagination getPage(int pageNo, int pageSize);

	public EventInfo findById(Long id);

	public EventInfo save(EventInfo bean);

	public EventInfo update(EventInfo bean);

	public EventInfo deleteById(Long id);
	
	public EventInfo[] deleteByIds(Long[] ids);
}