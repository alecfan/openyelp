package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.UserRole;
import com.openyelp.data.entity.UserInfo;

public interface UserService {
	/**加密方法*/
	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;	//盐长度
	public Pagination getPage(int pageNo, int pageSize);

	public UserInfo findById(Long id);

	public UserInfo save(UserInfo bean);
	/**
	 * 
	 * @param userid 用户id
	 * @param oldpassword 以前的密码
	 * @param password 新密码
	 * @return 修改成功后id>0
	 */
	public UserInfo updatePassword(Long userid,String oldpassword,String password);

	public boolean addRole(Long id, UserRole bean);

	public UserInfo update(UserInfo bean);

	public UserInfo deleteById(Long id);

	public UserInfo login(String username, String password, String macaddress);

	public UserInfo[] deleteByIds(Long[] ids);

	public UserInfo findByUsername(String username);
	
	public boolean checkPassword(UserInfo user,String oldPassword);

	public UserInfo updateUserLogin(UserInfo user);
}