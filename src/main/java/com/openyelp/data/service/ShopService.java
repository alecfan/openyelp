package com.openyelp.data.service;

import com.openyelp.annotation.RestFul;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.Shop;

@RestFul(value="ShopService",api=ShopService.class)
public interface ShopService {
	public Pagination getPage(int pageNo, int pageSize);

	public Shop findById(Integer id);

	public Shop save(Shop bean);
	public Shop addTag(Shop bean,String tag);
	public Shop deleteTag(Shop bean,String tag);

	public Shop update(Shop bean);

	public Shop deleteById(Integer id);
	
	public Shop[] deleteByIds(Integer[] ids);
}