package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.Talk;

public interface TalkService {
	public Pagination getPage(int pageNo, int pageSize);

	public Talk findById(Integer id);

	public Talk save(Talk bean);

	public Talk update(Talk bean);

	public Talk deleteById(Integer id);
	
	public Talk[] deleteByIds(Integer[] ids);

	public Talk add(Talk talk, int catalog, String city);
	
	
	public Pagination findByCity(int cityid,int category,int pageNo, int pageSize);

	public Pagination findByCity(int cityid,int category,long userid,int pageNo, int pageSize);

}