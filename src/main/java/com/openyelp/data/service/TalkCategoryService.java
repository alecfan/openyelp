package com.openyelp.data.service;

import java.util.List;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.TalkCategory;

public interface TalkCategoryService {
	public Pagination getPage(int pageNo, int pageSize);

	public TalkCategory findById(Integer id);

	public List<TalkCategory> findByCity(Integer id);

	public TalkCategory save(TalkCategory bean);

	public TalkCategory update(TalkCategory bean);

	public TalkCategory deleteById(Integer id);

	public TalkCategory[] deleteByIds(Integer[] ids);
}