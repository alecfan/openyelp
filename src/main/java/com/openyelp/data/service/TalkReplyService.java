package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.TalkReply;

public interface TalkReplyService {
	public Pagination getPage(int pageNo, int pageSize);

	
	public Pagination pageByTalk(int talkid,int pageNo, int pageSize);

	public TalkReply findById(Integer id);

	public TalkReply save(TalkReply bean);

	public TalkReply update(TalkReply bean);

	public TalkReply deleteById(Integer id);
	
	public TalkReply[] deleteByIds(Integer[] ids);
}