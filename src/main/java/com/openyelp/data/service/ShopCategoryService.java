package com.openyelp.data.service;

import java.util.List;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.ShopCategory;

public interface ShopCategoryService {
	public Pagination getPage(int pageNo, int pageSize);

	public ShopCategory findById(Integer id);

	public ShopCategory save(ShopCategory bean);

	public ShopCategory update(ShopCategory bean);

	public ShopCategory deleteById(Integer id);
	
	public ShopCategory[] deleteByIds(Integer[] ids);

	public List<ShopCategory> findByPid(int id);
}