package com.openyelp.data.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ada.common.security.Digests;
import com.ada.common.security.Encodes;
import com.openyelp.data.core.Finder;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.UserDao;
import com.openyelp.data.entity.UserRole;
import com.openyelp.data.entity.UserInfo;
import com.openyelp.data.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public UserInfo findById(Long id) {
		UserInfo entity = dao.findById(id);
		return entity;
	}

	@Transactional
	public UserInfo save(UserInfo bean) {
		UserInfo user=new UserInfo();
		List<UserInfo> us = dao.findByProperty("username", bean.getUsername());
		if(us!=null&&us.size()>0){
			user.setId(-1l);
		}else{
			entryptPassword(bean);
			user=dao.save(bean);
		}
		return user;
	}

	@Transactional
	public UserInfo update(UserInfo bean) {
		Updater<UserInfo> updater = new Updater<UserInfo>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

	@Transactional
	public UserInfo deleteById(Long id) {
		UserInfo bean = dao.deleteById(id);
		return bean;
	}

	@Transactional
	public UserInfo[] deleteByIds(Long[] ids) {
		UserInfo[] beans = new UserInfo[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private UserDao dao;

	@Autowired
	public void setDao(UserDao dao) {
		this.dao = dao;
	}

	@Transactional
	@Override
	public boolean addRole(Long id, UserRole bean) {
		boolean result=false;
		UserInfo entity = dao.findById(id);
		result=entity.getRoles().add(bean);
		return result;
	}

	@Override
	public UserInfo findByUsername(String username) {
		UserInfo result = null;
		Finder finder = Finder.create();
		finder.append("from UserInfo u where u.username ='" + username + "'");
		List<UserInfo> us = dao.find(finder);
		if (us != null && us.size() > 0) {
			result = us.get(0);
		}
		return result;
	}

	/**
	 * 设定安全的密码，生成随机的salt并经过1024次 sha-1 hash
	 */
	private void entryptPassword(UserInfo user) {
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		user.setSalt(Encodes.encodeHex(salt));

		byte[] hashPassword = Digests.sha1(user.getPlainPassword().getBytes(),
				salt, HASH_INTERATIONS);
		user.setPassword(Encodes.encodeHex(hashPassword));
	}

	/**
	 * 验证原密码是否正确
	 * 
	 * @param user
	 * @param oldPwd
	 * @return
	 */
	public boolean checkPassword(UserInfo user, String oldPassword) {
		byte[] salt = Encodes.decodeHex(user.getSalt());
		byte[] hashPassword = Digests.sha1(oldPassword.getBytes(), salt,
				HASH_INTERATIONS);
		if (user.getPassword().equals(Encodes.encodeHex(hashPassword))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 修改用户登录
	 * 
	 * @param user
	 */
	@Transactional
	public UserInfo updateUserLogin(UserInfo user) {
		user = dao.findById(user.getId());
		user.setLastDate(new Date());
		Integer times = user.getLogintimes();
		if (times == null) {
			times = 0;
		}
		times++;
		user.setLogintimes(times);
		return update(user);
	}

	@Transactional
	@Override
	public UserInfo login(String username, String password, String macaddress) {
		UserInfo result = null;
		Finder finder = Finder.create();
		finder.append("from UserInfo u where u.username ='" + username + "'");
		// finder.append("  and  u.password = '" + password + "'");
		List<UserInfo> us = dao.find(finder);
		if (us != null && us.size() > 0) {
			result = us.get(0);
			if (checkPassword(result, password)) {
				result.setMacaddress(macaddress);
				result.setLastDate(new Date());
				Integer logintime = result.getLogintimes();
				if (logintime == null) {
					logintime = 0;
				}
				logintime++;
				result.setLogintimes(logintime);
			} else {
				result = null;
			}

		}
		return result;
	}
	
	
	@Transactional
	@Override
	public UserInfo updatePassword(Long userid, String oldpassword,
			String password) {
		UserInfo user=	dao.findById(userid);
		if(user!=null){
			if (checkPassword(user, oldpassword)){
				user.setPlainPassword(password);
				entryptPassword(user);
			}else{
				user=new UserInfo();
				user.setId(-1l);
			}
		}else{
			user=new UserInfo();
			user.setId(-2l);
		}
		return user;
	}
}