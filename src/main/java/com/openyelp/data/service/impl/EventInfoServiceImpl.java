package com.openyelp.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.EventInfoDao;
import com.openyelp.data.entity.EventInfo;
import com.openyelp.data.service.EventInfoService;

@Service
@Transactional
public class EventInfoServiceImpl implements EventInfoService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public EventInfo findById(Long id) {
		EventInfo entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public EventInfo save(EventInfo bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public EventInfo update(EventInfo bean) {
		Updater<EventInfo> updater = new Updater<EventInfo>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public EventInfo deleteById(Long id) {
		EventInfo bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public EventInfo[] deleteByIds(Long[] ids) {
		EventInfo[] beans = new EventInfo[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private EventInfoDao dao;

	@Autowired
	public void setDao(EventInfoDao dao) {
		this.dao = dao;
	}
}