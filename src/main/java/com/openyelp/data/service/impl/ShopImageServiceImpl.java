package com.openyelp.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.ShopImageDao;
import com.openyelp.data.entity.ShopImage;
import com.openyelp.data.service.ShopImageService;

@Service
@Transactional
public class ShopImageServiceImpl implements ShopImageService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public ShopImage findById(Long id) {
		ShopImage entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public ShopImage save(ShopImage bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public ShopImage update(ShopImage bean) {
		Updater<ShopImage> updater = new Updater<ShopImage>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public ShopImage deleteById(Long id) {
		ShopImage bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public ShopImage[] deleteByIds(Long[] ids) {
		ShopImage[] beans = new ShopImage[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private ShopImageDao dao;

	@Autowired
	public void setDao(ShopImageDao dao) {
		this.dao = dao;
	}
}