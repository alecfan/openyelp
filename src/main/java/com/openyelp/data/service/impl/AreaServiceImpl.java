package com.openyelp.data.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Finder;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.AreaDao;
import com.openyelp.data.entity.Area;
import com.openyelp.data.service.AreaService;

@Service
@Transactional
public class AreaServiceImpl implements AreaService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public Area findById(Integer id) {
		Area entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public Area save(Area bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public Area update(Area bean) {
		Updater<Area> updater = new Updater<Area>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public Area deleteById(Integer id) {
		Area bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public Area[] deleteByIds(Integer[] ids) {
		Area[] beans = new Area[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private AreaDao dao;

	@Autowired
	public void setDao(AreaDao dao) {
		this.dao = dao;
	}
	@Transactional(readOnly = true)
	@Override
	public List<Area> findByLevel(Integer id) {
		// TODO Auto-generated method stub
		return dao.findByProperty("levelinfo", id);
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Area> findByParent(Integer id) {
		// TODO Auto-generated method stub
		return dao.findByProperty("parent.id", id);
	}

	@Override
	public List<Area> findByHot(Integer id) {
		List<Area>  result=null;
		Finder finder=Finder.create();
		finder.append("select h.area from AreaHot h");
		dao.find(finder, 0, id);
		result=(List<Area>) dao.find(finder, 0, id).getList();
		
		return result;
	}

	@Override
	public Area findByName(String name) {
		
		Area  resultone=null;

		List<Area>  result=null;
		Finder finder=Finder.create();
		finder.append(" from Area a  where a.name=:name");
		finder.setParam("name", name);
		result=dao.find(finder);
		if(result!=null&&result.size()>0){
			resultone=result.get(0);
		}	
		return resultone;
	}
}