package com.openyelp.data.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.ShopReviewDao;
import com.openyelp.data.entity.ShopReview;
import com.openyelp.data.service.ShopReviewService;

@Service
@Transactional
public class ShopReviewServiceImpl implements ShopReviewService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public ShopReview findById(Long id) {
		ShopReview entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public ShopReview save(ShopReview bean) {
    	if(bean.getShop()==null||bean.getUser()==null){
    		
    	}else{
    		bean.setAddDate(new Date());
    		bean.setLastDate(new Date());
    		dao.save(bean);
    	}
		return bean;
	}

    @Transactional
	public ShopReview update(ShopReview bean) {
		Updater<ShopReview> updater = new Updater<ShopReview>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public ShopReview deleteById(Long id) {
		ShopReview bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public ShopReview[] deleteByIds(Long[] ids) {
		ShopReview[] beans = new ShopReview[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private ShopReviewDao dao;

	@Autowired
	public void setDao(ShopReviewDao dao) {
		this.dao = dao;
	}
}