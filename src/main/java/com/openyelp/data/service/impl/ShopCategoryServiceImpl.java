package com.openyelp.data.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Finder;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.ShopCategoryDao;
import com.openyelp.data.entity.ShopCategory;
import com.openyelp.data.service.ShopCategoryService;

@Service
@Transactional
public class ShopCategoryServiceImpl implements ShopCategoryService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public ShopCategory findById(Integer id) {
		ShopCategory entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public ShopCategory save(ShopCategory bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public ShopCategory update(ShopCategory bean) {
		Updater<ShopCategory> updater = new Updater<ShopCategory>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public ShopCategory deleteById(Integer id) {
		ShopCategory bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public ShopCategory[] deleteByIds(Integer[] ids) {
		ShopCategory[] beans = new ShopCategory[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private ShopCategoryDao dao;

	@Autowired
	public void setDao(ShopCategoryDao dao) {
		this.dao = dao;
	}

	
	@Transactional(readOnly = true)
	@Override
	public List<ShopCategory> findByPid(int id) {
		Finder finder=Finder.create();
		finder.append("from ShopCategory s where s.parent.id=:pid");
		finder.setParam("pid",  id);
		return dao.find(finder);
	}
}