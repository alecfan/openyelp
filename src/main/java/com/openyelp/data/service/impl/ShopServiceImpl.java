package com.openyelp.data.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openyelp.data.core.Finder;
import com.openyelp.data.core.Pagination;
import com.openyelp.data.core.Updater;
import com.openyelp.data.dao.ShopAttrDao;
import com.openyelp.data.dao.ShopDao;
import com.openyelp.data.dao.ShopTagDao;
import com.openyelp.data.entity.Shop;
import com.openyelp.data.entity.ShopTag;
import com.openyelp.data.service.ShopService;

@Service
@Transactional
public class ShopServiceImpl implements ShopService {
	@Transactional(readOnly = true)
	public Pagination getPage(int pageNo, int pageSize) {
		Pagination page = dao.getPage(pageNo, pageSize);
		return page;
	}

	@Transactional(readOnly = true)
	public Shop findById(Integer id) {
		Shop entity = dao.findById(id);
		return entity;
	}

	@Transactional
	public Shop save(Shop bean) {
		
		Finder finder=Finder.create();
		finder.append("from Shop s where s.name=:name");
		finder.setParam("name", bean.getName());
		finder.append(" and s.gps =:gps");
		finder.setParam("gps", bean.getGps());
		dao.find(finder);
		
		dao.save(bean);
		
		
		
		return bean;
	}

	@Transactional
	public Shop update(Shop bean) {
		Updater<Shop> updater = new Updater<Shop>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

	@Transactional
	public Shop deleteById(Integer id) {
		Shop bean = dao.deleteById(id);
		return bean;
	}

	@Transactional
	public Shop[] deleteByIds(Integer[] ids) {
		Shop[] beans = new Shop[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private ShopDao dao;

	@Autowired
	private ShopAttrDao shopAttrDao;

	@Autowired
	private ShopTagDao shopTagDao;

	@Autowired
	public void setDao(ShopDao dao) {
		this.dao = dao;
	}

	@Override
	public Shop addTag(Shop bean, String tag) {

		if (bean.getId() == null||tag==null) {
			Shop shop = new Shop();
			shop.setId(-1);
			return shop;
		}
		bean = dao.findById(bean.getId());
		if (bean != null) {
			List<ShopTag> tags = shopTagDao.findByProperty("name", tag);
			if (tags != null && tags.size() > 0) {
				ShopTag tagx = tags.get(0);
			
				boolean isadd = bean.getTags().add(tagx);
				if(isadd){
					Integer size = tagx.getSize();
					if (size == null) {
						size = 0;
					}
					size++;
					tagx.setSize(size);
				}
			} else {
				ShopTag tagx = new ShopTag();
				tagx.setAddDate(new Date());
				tagx.setSize(1);
				tagx.setName(tag);
				shopTagDao.save(tagx);
				bean.getTags().add(tagx);
			}

		}
		return bean;
	}

	@Override
	public Shop deleteTag(Shop bean, String tag) {
		if (bean.getId() == null||tag==null) {
			Shop shop = new Shop();
			shop.setId(-1);
			return shop;
		}
		bean = dao.findById(bean.getId());
		if (bean != null) {
			List<ShopTag> tags = shopTagDao.findByProperty("name", tag);
			if (tags != null && tags.size() > 0) {
				ShopTag tagx = tags.get(0);
				boolean isdelete =bean.getTags().remove(tagx);
				if(isdelete){
					Integer size = tagx.getSize();
					if (size == null) {
						size = 1;
					}
					size--;
					if (size < 0) {
						size = 0;
					}
					tagx.setSize(size);
				}
			}

		}
		return bean;
	}

}