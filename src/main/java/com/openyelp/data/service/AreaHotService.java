package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.AreaHot;

public interface AreaHotService {
	public Pagination getPage(int pageNo, int pageSize);

	public AreaHot findById(Integer id);

	public AreaHot save(AreaHot bean);

	public AreaHot update(AreaHot bean);

	public AreaHot deleteById(Integer id);
	
	public AreaHot[] deleteByIds(Integer[] ids);
}