package com.openyelp.data.service;

import com.openyelp.data.core.Pagination;
import com.openyelp.data.entity.ShopReview;

public interface ShopReviewService {
	public Pagination getPage(int pageNo, int pageSize);

	public ShopReview findById(Long id);

	public ShopReview save(ShopReview bean);

	public ShopReview update(ShopReview bean);

	public ShopReview deleteById(Long id);
	
	public ShopReview[] deleteByIds(Long[] ids);
}