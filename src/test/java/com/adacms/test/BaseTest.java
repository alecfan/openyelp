package com.adacms.test;

import org.springframework.context.ApplicationContext;

import com.openyelp.data.apps.ObjectFactory;

import junit.framework.TestCase;

public class BaseTest  extends TestCase {

	ApplicationContext applicationContext;
	protected void setUp() throws Exception {
		super.setUp();
		applicationContext=ObjectFactory.get();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	public void testApp(){
		
	}
}
