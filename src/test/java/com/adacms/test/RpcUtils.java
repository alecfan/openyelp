package com.adacms.test;

import com.openyelp.client.RestFulClient;
import com.openyelp.data.service.UserService;

public class RpcUtils {

	//static String url = "http://openyelp.sturgeon.mopaas.com/rpc";
	static String url = "http://192.168.0.188:8080/openyelp/rpc";

	public static void main(String[] args) {
		
	}

	public static <T> T get(Class<T> classx) {
		T s = RestFulClient.getService(url, classx);
		return s;
	}
}
